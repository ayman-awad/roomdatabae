package com.noob.ayman.bbb.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.noob.ayman.bbb.DataBase.UserDataBase;
import com.noob.ayman.bbb.Model.User;
import com.noob.ayman.bbb.R;

import java.util.ArrayList;
import java.util.List;

public class HomeRegisterActivity extends AppCompatActivity {

    String name,email;
    TextView nameTV,emailTV;
    long id;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_register);

        nameTV = findViewById(R.id.nameTextView);

        Bundle intent = getIntent().getExtras();
        id = intent.getLong("idAddUser");
        User user = UserDataBase.getInstance(this).userDao().getUserById(id);
        nameTV.setText(user.getNameUser() + " "+ user.getEmailUser());


    }
}
