package com.noob.ayman.bbb.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "users")
public class User {


    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long idUser;

    @ColumnInfo(name = "name")
    private String nameUser;

    @ColumnInfo(name = "email")
    private String emailUser;

    public User(){

    }

    @NonNull
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(@NonNull long idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }


    @Override
    public String toString() {
        return "User{" +
                ", nameUser='" + nameUser + '\'' +
                ", emailUser='" + emailUser + '\'' +
                '}';
    }
}
