package com.noob.ayman.bbb.DataBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.noob.ayman.bbb.Converters;
import com.noob.ayman.bbb.Model.User;

import static com.noob.ayman.bbb.DataBase.UserDataBase.DATA_BASE_BERSION;

@Database(entities = User.class, version = DATA_BASE_BERSION)
@TypeConverters({Converters.class})
public abstract class UserDataBase extends RoomDatabase {
    public static final int DATA_BASE_BERSION = 1;
    public static final String DATA_BASE_NAME = "UserDataBase";

    public abstract UserDAO userDao();

    public static UserDataBase INSTANCE;

    public static UserDataBase getInstance(Context context) {
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    UserDataBase.class,
                    DATA_BASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }

    public static void DESTROY_INSTANCE() {
        INSTANCE = null;
    }
}
