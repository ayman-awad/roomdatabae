package com.noob.ayman.bbb.DataBase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.noob.ayman.bbb.Model.User;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface UserDAO {

    @Query("SELECT * FROM users")
   List<User> getAll();

    @Query("SELECT * FROM users WHERE id=:id")
    User getUserById(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(User user);

    @Delete
    void delete(User... user);

    @Update
    void update(User... user);

}
