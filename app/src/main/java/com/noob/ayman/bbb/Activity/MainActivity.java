package com.noob.ayman.bbb.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.noob.ayman.bbb.DataBase.UserDataBase;
import com.noob.ayman.bbb.Model.User;
import com.noob.ayman.bbb.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText nameET,emailET;

    Button save;
    Intent intent;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameET = findViewById(R.id.nameEditText);
        emailET = findViewById(R.id.emailEditText);

        save = findViewById(R.id.saveButton);
        save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String name,email;
        User user = new User();


        name = (String)nameET.getText().toString();
        email = (String)emailET.getText().toString();

        user.setNameUser(name);
        user.setEmailUser(email);



      long id =  UserDataBase.getInstance(this).userDao().insert(user);
      Log.d("test id", String.valueOf(id));
        Toast.makeText(getApplicationContext(),"SAVE USER",Toast.LENGTH_LONG).show();
        intent = new Intent(this, HomeRegisterActivity.class);
        intent.putExtra("idAddUser",id);
        startActivity(intent);

    }

}
